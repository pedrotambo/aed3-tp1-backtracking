# source: http://danishmujeeb.com/blog/2014/01/basic-sorting-algorithms-implemented-in-python

import random
import heapq
import timeit
import numpy as np

def bubble_sort(items):
    """ Implementation of bubble sort """
    for i in range(len(items)):
        for j in range(len(items)-1-i):
            if items[j] > items[j+1]:
                items[j], items[j+1] = items[j+1], items[j] # Swap!


def insertion_sort(items):
    """ Implementation of insertion sort """
    for i in range(1, len(items)):
        j = i
        while j > 0 and items[j] < items[j-1]:
            items[j], items[j-1] = items[j-1], items[j]
            j -= 1

def merge_sort(items):
    """ Implementation of mergesort """
    if len(items) > 1:

        mid = len(items) / 2        # Determine the midpoint and split
        left = items[0:mid]
        right = items[mid:]

        merge_sort(left)            # Sort left list in-place
        merge_sort(right)           # Sort right list in-place

        l, r = 0, 0
        for i in range(len(items)):     # Merging the left and right list

            lval = left[l] if l < len(left) else None
            rval = right[r] if r < len(right) else None

            if (lval and rval and lval < rval) or rval is None:
                items[i] = lval
                l += 1
            elif (lval and rval and lval >= rval) or lval is None:
                items[i] = rval
                r += 1
            else:
                print lval, rval, l
                raise Exception('Could not merge, sub arrays sizes do not match the main array')



def quick_sort(items):
    """ Implementation of quick sort """
    if len(items) > 1:
        pivot_index = len(items) / 2
        smaller_items = []
        larger_items = []

        for i, val in enumerate(items):
            if i != pivot_index:
                if val < items[pivot_index]:
                    smaller_items.append(val)
                else:
                    larger_items.append(val)

        quick_sort(smaller_items)
        quick_sort(larger_items)
        items[:] = smaller_items + [items[pivot_index]] + larger_items


def heap_sort(items):
    """ Implementation of heap sort """
    heapq.heapify(items)
    items[:] = [heapq.heappop(items) for i in range(len(items))]


if __name__ == '__main__':

    delimiter = ';'
    sort_functions = {
        'bubble_sort': bubble_sort,
        'insertion_sort': insertion_sort,
        # 'merge_sort': merge_sort,
        'quick_sort': quick_sort,
        'heap_sort': heap_sort
    }

    cases = ['random','asc','des']


    # Abre el archivo donde va a ir guardando datos
    with open('data.csv','w+') as file:

        # Escribe el head del csv. O sea, los nombres de las columnas
        file.write(delimiter.join(['algoritmo','caso','n','comienzo','fin','duracion']) + '\n')
        
        # Itera sobre el diccionario de <nombre, algoritmo>
        for algorithm, sort in sort_functions.iteritems():

            for case in cases:
                # Para cada algoritmo y caso realiza muchas corridas, en cada corrida la lista 
                # tiene un elemento mas arrancando por 0
                for i in range(10000):

                    # Para cada cantidad de elementos, se realizan muchas corridas para promediarlas.
                    for j in range(100):
                        
                        # Genera una nueva lista a ordenar con i cantidad de elementos
                        if case == 'random':
                            items = [random.randint(-50, 100) for c in range(i)]
                        elif case == 'asc':
                            items = range(i)
                        elif case == 'des':
                            items = range(i)
                            items.reverse()
                        
                        # Corre el algoritmo de sorting midiento tiempos
                        # Nota: Esto no mide tiempo de CPU del proceso, mide tiempo global
                        # Deberia utilizarse una libreria que mida tiempo de uso del CPU.
                        start_time = timeit.default_timer()
                        sort(items)
                        end_time = timeit.default_timer()
                        elapsed_time = end_time - start_time
                        numbers = [i,start_time,end_time,elapsed_time]

                        # Agrega nuevos datos al csv
                        file.write(delimiter.join([algorithm,case] + map(str, numbers)) + '\n')
