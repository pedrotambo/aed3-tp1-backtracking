# source: http://danishmujeeb.com/blog/2014/01/basic-sorting-algorithms-implemented-in-python

import random
import heapq
import timeit
import numpy as np
import subprocess

import subprocess

#print("Running python script...")
#pname = "./main"
#
#myinput = open('input', 'r')
#myoutput = open('ouput', 'w')
#
#mycomment = open('cerr', 'w')
#
#p = subprocess.Popen(pname, stdin=myinput, stdout=myoutput, stderr=mycomment)
#p.wait()
#
#
#file = open('cerr')
#o = file.read()
#print(o)
#file.close()
#
#myinput.close()
#myoutput.close()




# requiere que rinput este ya creado, crea un archivo output con la cantidad de ticks que tardo en correr el algoritmo sin poda
def slow_mode():
    pname = "./main"
    rinput = open('rinput', 'r')
    routput = open('routput', '+w')
    routput.truncate(0)
    p = subprocess.Popen([pname, "1"], stdin=rinput, stdout=routput)
    p.wait()

    routput = open('routput', 'r')
    s = routput.read()
    
    routput.close()
    return s


def fast_mode():
    pname = "./main"
    rinput = open('rinput', 'r')
    routput = open('routput', '+w')
    routput.truncate(0)
    p = subprocess.Popen(pname, stdin=rinput, stdout=routput)
    p.wait()

    routput = open('routput', 'r')
    s = routput.read()
    
    routput.close()
    return s


def generate_rinput(i,a):
    randinput = open('rinput', 'w')
    randinput.truncate(0)
    #x = int(input('Cuantos agentes? '))
    agents = i
    #y = int(input('Cuantas encuestas? '))
    quests = a
    randinput.write("{0} {1} \n".format(agents, quests))
    for i in range(0, quests):
        a = np.random.randint(1, agents+1)
        b = np.random.randint(1,agents+1)
        sign = np.random.uniform()
        if(sign > 0.5):
            b = -b
        randinput.write("{0} {1} \n".format(a,b))
    randinput.write("0 0 \n")
    randinput.close()








if __name__ == '__main__':

    delimiter = ';'
    solve_functions = {
        'slow': slow_mode,
        'fast': fast_mode
    }

    generate_rinput(30,80)


    cases = {
        'very_small': [3, 10],
        'small': [10, 30],
        'big': [12, 30],
        'very_big': [15, 40] 
    }


    #cases = ['random','asc','des']


    # Abre el archivo donde va a ir guardando datos
    with open('data.csv','w+') as file:

        # Escribe el head del csv. O sea, los nombres de las columnas
        file.write(delimiter.join(['modo','agentes','encuestas','duracion']) + '\n')
        
        # Itera sobre el diccionario de <modo, algoritmo>
        for mode, function in solve_functions.items():
            for case in cases:
                # Para cada algoritmo y caso realiza muchas corridas, en cada corrida la lista
                agents = cases[case][0]
                quests = cases[case][1]
                
                for i in range(1):
                    generate_rinput(agents, quests)
                    time_span = function()
                    print(time_span)
                    a = str(agents)
                    q = str(quests)
                    file.write(delimiter.join([mode, a, q, time_span]) + '\n')
