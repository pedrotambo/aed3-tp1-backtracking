#!/usr/bin/env python3.4.3

import subprocess

print("Running python script...")
pname = "./main"

myinput = open('input', 'r')
myoutput = open('ouput', 'w')

mycomment = open('cerr', 'w')

p = subprocess.Popen(pname, stdin=myinput, stdout=myoutput, stderr=mycomment)
p.wait()


file = open('cerr')
o = file.read()
print(o)
file.close()

myinput.close()
myoutput.close()

