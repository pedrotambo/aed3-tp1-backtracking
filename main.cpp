#include <cmath>
#include <cstdio>
#include <iostream>
#include <algorithm>
#include <chrono>
#include <vector>
#include <string>
#include "helper/tiempo.h"
#include "helper/algorithms.h"
#include "helper/func.h"


//#define EXP
//#define DEBUG
#ifdef DEBUG
//Aca se puede incluir cualquier cosa que consideren que necesitan para debug
//Este método de debuggeo también se aplica en algorithms.cpp, en caso de necesitarlo para los algoritmos definidos en ese archivo.
    #include "helper/debug.h"
#endif

using namespace std;


int main(int argc, char **argv) {

    /* Por default el programa SIN parámetros corre la versión optimizada (con podas)
        Para correr el algoritmo con la versión sin poda, basta pasarle el parámetro 1
        "main 1 < input"
                        */
    bool fast_mode = true;
    if( argc > 2 || (argc == 2 && (string)argv[1] != "1")){
        cerr << "El programa no se ejecuto correctamente. Lea el archivo README" << endl;    
        return 0;
    } else if((argc == 2 && (string)argv[1] == "1")){
        // Cambio a modo low (sin podas)   
        fast_mode = false;
    }
    
    int i, a;

    cin >> i >> a;

    while ( !(i == 0 && a == 0) ){
        
        vector<survey> survey(a);
        for(int j = 0; j < a; j++){
            cin >> survey[j].x >> survey[j].y;
        }


        #ifdef DEBUG
            show_surveys(survey, fast_mode);
            //
            //
        #endif
        
        // Una vez leídos los datos del cada set, se procede a resolver el algoritmo,
        // Tomo el tiempo antes de iniciar.
        unsigned long long start, end, time_span;
        MEDIR_TIEMPO_START(start)

        vector<int> solution(i+1,-1);
        int stage = 1;
        int max_size = 0;
        int partial_size = 0;
        vector<int> final_set(i+1,-1);

        if(fast_mode){
            surveyRec(solution, stage, survey, final_set, max_size, partial_size);
        } else {
            surveyRecSlow(solution, stage, survey, final_set, max_size, partial_size);
        }
        
        // Una vez que terminó, tomo el tiempo cuando finalizó.
        MEDIR_TIEMPO_STOP(end)
        time_span = end - start;
        (void) time_span;

        
        #ifdef DEBUG
            show_time(time_span);
            show_sol(final_set);
        #endif
        
       
        #ifdef EXP
            cout << time_span;
        #else
            cout << max_size << endl;        
        #endif

        cin >> i >> a;

    }

}