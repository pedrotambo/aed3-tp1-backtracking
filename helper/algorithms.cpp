#include "algorithms.h"

//#define DEBUG
#ifdef DEBUG
    //#include "debug.h"
#endif

//#define COUNTER
#ifdef COUNTER
    unsigned int count = 0;      
#endif

//#define MAX_STAGE(v, stage) (v.size()-1)

bool validate(vector<int> &solution, int stage, vector<survey> &survey, int &max_size, int partial_size){
    
    
    if (max_size > partial_size + ((int) solution.size()-1 - stage)){
        // Puedo podar, por esta rama por mas que todos los niveles de abajo sean consistentes no voy a superar el max;
        // ALTA PODA GUACH
        return false;
    }
   
    int res = true;

    for (int k = 0; k < (int) survey.size() && res; k++){
        // opinion de A hacia B, donde A = survey[k].x   y   B = |survey[k].y|:
        int A = survey[k].x;
        int B = survey[k].y;
        if (solution[A] == 1){
            // A esta en la solucion parcial, me importa su opinion para la consistencia.
            if(A == stage){
                // Opinion de stage = A
                if (B > 0){
                    /*  La opinion de A sobre B es favorable, entonces:
                        El conjunto es confiable si:
                                - B esta en el conjunto parcial y B <= A
                                            ´o
                                - B > A (todavia no decidi si va a estar o no)
                    */
                    res = ( B <= A )? solution[B] == 1 : true;

                } else {
                    /*  La opinion de A sobre B es desfavorable, entonces:
                        El conjunte es confiable si:
                                - B no esta en el conjunto y B <= A
                                - B > A
                    */
                    res = ( abs(B) <= stage )? solution[abs(B)] == 0 : true;

                }
            } else {
                // A != STAGE
                if ( abs(B) == stage ){
                    /* A opina de stage = B
                        Si opina positivo y stage no esta, ERROR.
                        Si opina negativo y stage esta, ERROR.
                        Casos contrarios es consistente. */
                    res = (B < 0)? solution[stage] == 0 : solution[stage] == 1;
                } 
            }
        } else {
            // k no esta en la solucion parcial, no me interesa su opinion
        }  
    }
    return res;
}

void update(vector<int> &solution, vector<survey> &survey, vector<int> &final_set, int &max_size, int partial_size){
    #ifdef DEBUG

        show_sol_partial(solution);
    #endif


    if(partial_size > max_size){
        for(int i = 1; i < (int) solution.size(); i++){
            final_set[i] = solution[i];
        }
        max_size = partial_size;
    }

}


void surveyRec(vector<int> &solution, int stage, vector<survey> &survey, vector<int> &final_set, int &max_size, int partial_size){
    
    if (stage > (int) solution.size()-1){
        return;
    }

    for (int i = 0; i < 2; i++){
        solution[stage] = i;
        if(i == 1) partial_size++;
        if(validate(solution, stage, survey, max_size, partial_size)){
            if(stage == (int) solution.size() - 1){
                update(solution, survey, final_set, max_size, partial_size);
            } else {
                surveyRec(solution, stage + 1, survey, final_set, max_size, partial_size);
            }
        }
    }
}




void surveyRecSlow(vector<int> &solution, int stage, vector<survey> &survey, vector<int> &final_set, int &max_size, int partial_size){
    int i = 0;
    if (stage > (int) solution.size()-1){
        return;
    }

    do{
        solution[stage] = i;
        if(true){          

            if (i == 1) partial_size++;
            if(stage == (int) solution.size() - 1 ){

                #ifdef COUNTER
                    count++;
                    cerr << count << endl;
                #endif

                if (validate(solution, solution.size() - 1, survey, max_size, partial_size)){
                    update(solution, survey, final_set, max_size, partial_size);
                }
            } else{
                surveyRecSlow(solution, stage + 1, survey, final_set, max_size, partial_size);
            }
        }
        i++;   
    } while(solution[stage] != 1);

    solution[stage] = -1;
}