CC = g++
CFLAGS = -c -Wall
OBJECTS = main.o algorithms.o


all: main 

main: $(OBJECTS)
	$(CC) $(OBJECTS) -o main

main.o: main.cpp
	$(CC) $(CFLAGS) main.cpp

algorithms.o: ./helper/algorithms.cpp
	$(CC) $(CFLAGS) ./helper/algorithms.cpp

clean:
	rm -f *.o
	rm -f main
	rm -f output