#ifndef __ALGORITHMS_H__
#define __ALGORITHMS_H__

#include <vector>
#include <cstdio>
#include <iostream>

using namespace std;

struct survey {
    int x;
    int y;
    survey(): x(0), y(0){}
    survey(int x, int y): x(x), y(y){}
    ~survey(){}
};



bool validate(vector<int> &solution, int stage, vector<survey> &survey);

void update(vector<int> &solution, vector<survey> &survey, vector<int> &final_set, int &max_size, int partial_size);

void surveyRec(vector<int> &solution, int stage, vector<survey> &survey, vector<int> &final_set, int &max_size, int partial_size);

void surveyRecSlow(vector<int> &solution, int stage, vector<survey> &survey, vector<int> &final_set, int &max_size, int partial_size);




#endif /* !__ALGORITHMS_H__ */