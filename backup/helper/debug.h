#ifndef __DEBUG_H__
#define __DEBUG_H__

void show_time(unsigned long long time_span){
	cerr << "Cantidad de clocks: " << time_span << endl;
}


void show_sol(vector<int> &final_set){
    cerr << "Solucion FINAL AUXILIAR: ";

    int size = 0;
    for(int i = 1; i < (int) final_set.size(); i++){
            if(final_set[i] == 1){
                size++;
                cerr << i << " ";  
            } 

    }
    cerr << endl << "Tamano: " << size << endl;
}

void show_surveys(vector<survey> &survey, bool fast_mode){
    cerr << "----------------------------------------------" << endl;
    if(fast_mode){
    	cerr << "Running fast mode" << endl;
    } else {
    	cerr << "Running low mode" << endl;
    }
    cerr << " - Resultados de la encuesta: " << endl;
    for(int i = 0; i < (int) survey.size(); i++){
        cerr << survey[i].x << " " << survey[i].y << endl;
    }
}


void show_sol_partial(vector<int> &solution){
    
    cerr << "Solucion parcial: ";
    for(int i = 1; i < (int) solution.size(); i++){
        if(solution[i] == 1) cerr << i << " ";
    }
    cerr << endl;
}



#endif /* !__DEBUG_H__ */
