import numpy as np


randinput = open('rinput', 'w')
randinput.truncate(0)
x = int(input('Cuantos agentes? '))
agents = x
y = int(input('Cuantas encuestas? '))
quests = y
randinput.write("{0} {1} \n".format(agents, quests))
for i in range(0, quests):
	a = np.random.randint(1, agents+1)
	b = np.random.randint(1,agents+1)
	sign = np.random.uniform()
	if(sign > 0.5):
		b = -b
	randinput.write("{0} {1} \n".format(a,b))
randinput.write("0 0 \n")

randinput.close()
